const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv/config');

const userRouter = require('./routes/userRoutes');
const passRouter = require('./routes/passRoutes');
const newRouter = require('./routes/newRoutes');
const querieRouter = require('./routes/querieRoutes');
const emailRouter = require('./routes/emailRoutes');

const User = require('./models/users');
const Querie = require('./models/queries');
const Password = require('./models/passwords');

const app = express();
const PORT = 5000;

//MIDDLEWARES
// app.use(cors({ origin: true, credentials: true }));
app.use(bodyParser.json());
app.use('/user', userRouter);
app.use('/password', passRouter);
app.use('/new', newRouter);
app.use('/querie', querieRouter);
app.use('/email', emailRouter);
app.use(cors({
    origin: "*",
}));

//CONNECT
mongoose.connect(process.env.DB_CONNECTION, () => {
    console.log('Connected to DB');
});

//LISTEN
app.listen(PORT, () => console.log(`Server running on port: http://localhost:${PORT}`));

// const NuevosUsuarios = [{
//     user: "ahlaulhe",
//     password: "admin123",
//     passwordConfirm: "admin123",
//     email: "ahlaulhe@gmail.com",
//     pin: "1234",
// },
// {
//     user: "profesor",
//     password: "admin123",
//     passwordConfirm: "admin123",
//     email: "profesor@gmail.com",
//     pin: "3344",
// }]

// User.insertMany(NuevosUsuarios).then(value => {
//     console.log("Saved Successfuly");
// })
//     .catch(error => {
//         console.log(error);
//     })
// const NuevaQuerie = [{
//     userId: "628904472b3716ef3e0be0da",
//     title: "duda sobre javascript",
//     description: "duda sobre javascript",
//     comments: [{
//         userId: "628904472b3716ef3e0be0da",
//         comment: "duda sobre javascript"
//     }]
// }]
// const NuevoPassword = [{
//     keyword: "Instagram",
//     genPassword: "1234asdfASDF$#@!",
//     userId: "628904472b3716ef3e0be0da"
// }]

// Querie.insertMany(NuevaQuerie).then(value => {
//     console.log("Saved Successfuly");
// })
//     .catch(error => {
//         console.log(error);
//     })

// Password.insertMany(NuevoPassword).then(value => {
//     console.log("Saved Successfuly");
// })
//     .catch(error => {
//         console.log(error);
//     })

// mongodb://localhost:27017/?serverSelectionTimeoutMS=5000&connectTimeoutMS=10000&3t.uriVersion=3&3t.connection.name=New+Connection&3t.alwaysShowAuthDB=true&3t.alwaysShowDBFromUserRole=true