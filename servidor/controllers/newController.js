const NewsAPI = require('newsapi');
const newsapi = new NewsAPI('f390ac9aeb1346fc84ec0651225d939e');

const catchAsync = require('../utils/catchAsync');
const AppError = require('../utils/appError');
// To query /v2/top-headlines
// All options passed to topHeadlines are optional, but you need to include at least one of them

exports.getAllNews = catchAsync(async (req, res, next) => {
  const news = await newsapi.v2.everything({
    language: 'en',
    sortBy: 'relevancy',
    q: 'JavaScript',
  })
  res.status(200).json({
    status: 'success',
    results: news.length,
    data: {
      news
    }
  })
});

exports.getNewsByCategory = catchAsync(async (req, res, next) => {
  const news = await newsapi.v2.topHeadlines({
    language: 'en',
    sortBy: 'relevancy',
    category: `${req.params.category}`,
  });

  res.status(200).json({
    status: 'success',
    data: {
      news
    }
  });
});

// newsapi.v2.topHeadlines({
//     sources: 'bbc-news,the-verge',
//     q: 'bitcoin',
//     category: 'business',
//     language: 'en',
//     country: 'us'
// }).then(response => {
//     console.log(response);
/*
  {
    status: "ok",
    articles: [...]
  }
*/
// });
// To query /v2/everything
// You must include at least one q, source, or domain
// newsapi.v2.everything({
//     q: 'bitcoin',
//     sources: 'bbc-news,the-verge',
//     domains: 'bbc.co.uk, techcrunch.com',
//     from: '2017-12-01',
//     to: '2017-12-12',
//     language: 'en',
//     sortBy: 'relevancy',
//     page: 2
// }).then(response => {
//     console.log(response);
/*
  {
    status: "ok",
    articles: [...]
  }
*/
// });
// To query sources
// All options are optional
// newsapi.v2.sources({
//     category: 'technology',
//     language: 'en',
//     country: 'us'
// }).then(response => {
//     console.log(response);
/*
  {
    status: "ok",
    sources: [...]
  }
*/
// });