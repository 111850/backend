const mongoose = require('mongoose');
const express = require('express');
const nodemailer = require('nodemailer');

const catchAsync = require('../utils/catchAsync');
const User = require('../models/users');

const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'passafeee@gmail.com',
        pass: 'eywpnkcaztlvcnxo'
    }
});

exports.sendPassword = catchAsync(async (req, res, next) => {
    const userInfo = await User.findOne({ email: req.params.email });
    const newEmail = {
        from: 'passafeee@gmail.com',
        to: req.params.email,
        subject: 'Password recover | paSSafe',
        text: `Your password is ${userInfo.passwordConfirm}`
    };
    transporter.sendMail(newEmail, function (err, info) {
        if (err) {
            console.log(err);
        } else {
            console.log('Email sent: ' + info.response)
        }
    });

    res.status(201).json({
        status: 'success',
        data: null
    });
});