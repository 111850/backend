const mongoose = require('mongoose');
const express = require('express');

const Queries = require('../models/queries');
const catchAsync = require('../utils/catchAsync');

exports.getAllQueries = catchAsync(async (req, res, next) => {
    const queries = await Queries.find();

    res.status(200).json({
        status: 'success',
        results: queries.length,
        data: {
            queries
        }
    })
});
exports.getQuerie = catchAsync(async (req, res, next) => {
    const querie = await Queries.findById(req.params.id);
    // Tour.findOne({ _id: req.params.id })

    if (!querie) {
        return next(new AppError('No querie found with that ID', 404));
    }

    res.status(200).json({
        status: 'success',
        data: {
            querie
        }
    });
});
exports.createQuerie = catchAsync(async (req, res, next) => {
    const newQuerie = await Queries.create(req.body);

    res.status(201).json({
        status: 'success',
        data: {
            querie: newQuerie
        }
    });
});

exports.updateQuerie = catchAsync(async (req, res, next) => {
    const querie = await Queries.findByIdAndUpdate(req.params.id, req.body, {
        runValidators: true
    });

    if (!querie) {
        return next(new AppError('No querie found with that ID', 404));
    }

    res.status(200).json({
        status: 'success',
        data: {
            querie
        }
    });
});


exports.deleteQuerie = catchAsync(async (req, res, next) => {
    const querie = await Queries.findByIdAndDelete(req.params.id);

    if (!querie) {
        return next(new AppError('No querie found with that ID', 404));
    }

    res.status(204).json({
        status: 'success',
        data: null
    });
});