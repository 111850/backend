const jwt = require('jsonwebtoken');
const User = require('./../models/users');
const catchAsync = require('./../utils/catchAsync');
const AppError = require('./../utils/appError');

const signToken = id => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
    });
}

exports.signup = catchAsync(async (req, res, next) => {
    const newUser = await User.create({
        user: req.body.user,
        email: req.body.email,
        password: req.body.password,
        passwordConfirm: req.body.passwordConfirm,
        pin: req.body.pin,
    });

    const token = signToken(newUser._id);

    res.status(201).json({
        status: 'success',
        token,
        data: {
            user: newUser
        }
    });
});

exports.login = catchAsync(async (req, res, next) => {
    const { email, password } = req.body;
    // Comprobamos si el mail y la contraseña existen en la base de datos
    if (!email || !password) {
        return next(new AppError('Please enter your email and password', 400));
    }
    // Comprobamos si el usuario y la contraseña son correctas
    const user = await User.findOne({ email }).select('+password');
    // || !(await user.correctPassword(password, user.passwordConfirm))
    if (!user || user.passwordConfirm != password) {
        return next(new AppError('Incorrect mail or password', 401));
    }
    // console.log(user);
    // En caso de pasar las dos verificaciones anteriores, mandamos el token
    const token = signToken(user._id);
    res.status(200).json({
        status: 'success',
        token,
        data: {
            id: user._id
        }
    });
});

exports.confirmPin = catchAsync(async (req, res, next) => {
    const { userId, pin } = req.body;

    if (!userId || !pin) {
        return next(new AppError('Please enter your id and pin', 400));
    }

    const user = await User.findOne({ userId }).select('+pin');

    if (!user || user.pin != pin) {
        return next(new AppError('Incorrect pin', 401))
    }

    res.status(200).json({
        status: 'success',
        data: user
    });
})