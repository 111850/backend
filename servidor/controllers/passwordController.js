const mongoose = require('mongoose');
const express = require('express');
const CryptoJS = require('crypto-js');
const AES = require('crypto-js/aes');
const Utf8 = require('crypto-js/enc-utf8');

const catchAsync = require('./../utils/catchAsync');
const AppError = require('./../utils/appError');
const Password = require('./../models/passwords');

exports.getAllPasswords = catchAsync(async (req, res, next) => {
    const passwords = await Password.find();

    res.status(200).json({
        status: 'success',
        results: passwords.length,
        data: {
            passwords
        }
    })
});

// exports.getPassword = catchAsync(async (req, res, next) => {
//     userId = req.params.id;
//     const passwords = await Password.find({ userId });
//     // Tour.findOne({ _id: req.params.id })

//     if (!passwords) {
//         return next(new AppError('No passwords found with that ID', 404));
//     }

//     res.status(200).json({
//         status: 'success',
//         data: {
//             passwords
//         }
//     });
// });

exports.getPassword = catchAsync(async (req, res, next) => {
    userId = req.params.id;
    const passwords = await Password.find({ userId });
    const passphrase = "youcan'tdecryptthis"
    const decryptedPasswords = [];

    if (!passwords) {
        return next(new AppError('No passwords found with that ID', 404));
    }

    passwords.forEach(element => {
        const bytes = AES.decrypt(element.genPassword, passphrase);
        const originalText = bytes.toString(Utf8);
        decryptedPasswords.push({
            _id: element._id,
            keyword: element.keyword,
            genPassword: originalText,
            userId: element.userId
        });
    });

    res.status(200).json({
        status: 'success',
        data: {
            decryptedPasswords
        }
    });
});

// exports.createPassword = catchAsync(async (req, res, next) => {
//     const password = await Password.create(req.body);
//
//     res.status(201).json({
//         status: 'success',
//         data: {
//             password
//         }
//     });
// });

exports.createPassword = catchAsync(async (req, res, next) => {
    const passphrase = "youcan'tdecryptthis"
    const password = await Password.create({
        keyword: req.body.keyword,
        genPassword: CryptoJS.AES.encrypt(req.body.genPassword, passphrase).toString(),
        userId: req.body.userId
    });
    //genPassword

    res.status(201).json({
        status: 'success',
        data: {
            password
        }
    });
});

exports.updatePassword = catchAsync(async (req, res, next) => {
    const password = await Password.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });

    if (!password) {
        return next(new AppError('No password found with that ID', 404));
    }

    res.status(200).json({
        status: 'success',
        data: {
            password
        }
    });
});

exports.deletePassword = catchAsync(async (req, res, next) => {
    const password = await Password.findByIdAndDelete(req.params.id);

    if (!password) {
        return next(new AppError('No password found with that ID', 404));
    }

    res.status(204).json({
        status: 'success',
        data: null
    });
});