const mongoose = require('mongoose');
const express = require('express');

const User = require('./../models/users');
const catchAsync = require('./../utils/catchAsync');

exports.getAllUsers = catchAsync(async (req, res, next) => {
    const users = await User.find();

    res.status(200).json({
        status: 'success',
        results: users.length,
        data: {
            users
        }
    })
});

exports.getUser = catchAsync(async (req, res, next) => {
    const user = await User.findById(req.params.id);
    // Tour.findOne({ _id: req.params.id })

    if (!user) {
        return next(new AppError('No user found with that ID', 404));
    }

    res.status(200).json({
        status: 'success',
        data: {
            user
        }
    });
});

exports.createUser = (req, res) => {
    res.status(500).json({
        status: 'error',
        message: 'not yet implemented',
    });
};
// might be necessary in case of forgetting the password
exports.updateUser = (req, res) => {
    res.status(500).json({
        status: 'error',
        message: 'not yet implemented',
    });
};

exports.deleteUser = (req, res) => {
    res.status(500).json({
        status: 'error',
        message: 'not yet implemented',
    });
};
