const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');

const userSchema = mongoose.Schema({
    user: {
        type: String,
        required: [true, 'Porfavor ingrese su nombre']
    },
    password: {
        type: String,
        required: [true, 'Porfavor ingrese una contraseña'],
        minlength: 8,
        select: false
    },
    passwordConfirm: {
        type: String,
        required: [true, 'Porfavor ingrese la contraseña nuevamente'],
        validate: {
            validator: function (el) {
                return el === this.password;
            },
            message: 'Las contraseñas deben coincidir'
        }
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Porfavor ingrese un mail valido']
    },
    pin: {
        type: Number,
        required: [true, 'Porfavor ingrese un pin valido'],
        minlength: 6,
    }
});

userSchema.pre('save', async function (next) {
    if (!this.isModified('password')) return next();

    this.password = await bcrypt.hash(this.password, 12);

    this.passwordConfirm = undefined;
    next();
});

userSchema.methods.correctPassword = async function (candidatePassword, userPassword) {
    return await bcrypt.compare(candidatePassword, userPassword);
}

const User = mongoose.model('User', userSchema);

module.exports = User;