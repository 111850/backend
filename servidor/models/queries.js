const mongoose = require('mongoose');

const commentSchema = mongoose.Schema({
    userId: String,
    comment: {
        type: String,
    }
})

const querieSchema = mongoose.Schema({
    userId: String,
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    comments: [commentSchema],

});

const Queries = mongoose.model('Queries', querieSchema);

module.exports = Queries;