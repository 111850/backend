const mongoose = require('mongoose');
// const bcrypt = require('bcryptjs');

const passwordSchema = mongoose.Schema({
    keyword: {
        type: String,
        required: true,
    },
    genPassword: {
        type: String,
        required: true,
        unique: true,
    },
    userId: {
        type: String,
        required: true,
    }
});

// userSchema.pre('save', async function (next) {
//     if (!this.isModified('password')) return next();

//     this.password = await bcrypt.hash(this.password, 12);

//     this.passwordConfirm = undefined;
//     next();
// });

const Password = mongoose.model('Password', passwordSchema);

module.exports = Password;