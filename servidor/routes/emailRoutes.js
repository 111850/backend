const mongoose = require('mongoose');
const express = require('express');
const emailController = require('../controllers/emailController')

const router = express.Router();

router.route('/:email').post(emailController.sendPassword);

module.exports = router;