const mongoose = require('mongoose');
const express = require('express');
const newController = require('./../controllers/newController');

const router = express.Router();

router.route('/').get(newController.getAllNews);
router.route('/:category').get(newController.getNewsByCategory);

module.exports = router;