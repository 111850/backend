const mongoose = require('mongoose');
const express = require('express');
const querieController = require('../controllers/querieController');

const router = express.Router();

router.route('/').get(querieController.getAllQueries).post(querieController.createQuerie);
router.route('/:id').get(querieController.getQuerie).patch(querieController.updateQuerie).delete(querieController.deleteQuerie);

module.exports = router;